package com.example.minesweeper

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import timber.log.Timber
import java.io.BufferedReader
import java.io.File

class UserStats {
    fun readUserStats(userStatsFilePath: String): Map<String, Any> {

        var bestTime: Any = "-"
        var winCount: Any = 0
        var lossCount: Any = 0

        val fileObject = File(userStatsFilePath)
        if (!fileObject.exists()) {
            Timber.i("File  $userStatsFilePath does not exist!")
            fileObject.createNewFile()
            val defaultUserStats = mutableMapOf("bestTime" to bestTime, "winCount" to winCount, "lossCount" to lossCount)
            fileObject.writeText(GsonBuilder().create().toJson(defaultUserStats))
        } else {
            Timber.i("File  $userStatsFilePath exists!")
            val bufferedReader: BufferedReader = fileObject.bufferedReader()
            val currentUserStatsString = bufferedReader.use { it.readText() }
            Timber.i("User stats in string format: $currentUserStatsString")
            val currentUserStats = Gson().fromJson(currentUserStatsString, JsonObject::class.java)
            Timber.i("Current user stats from GSON: $currentUserStats")

            bestTime = currentUserStats.get("bestTime")
            winCount = currentUserStats.get("winCount")
            lossCount = currentUserStats.get("lossCount")
        }

        return mutableMapOf("bestTime" to bestTime, "winCount" to winCount, "lossCount" to lossCount)
    }

    fun writeUserStats(userStatsFilePath: String, winCount: Any? = null, lossCount: Any? = null, bestTime: Any? = null) {
        val previousUserStats = readUserStats(userStatsFilePath)

        val newUserStats = mapOf(
            "bestTime" to (bestTime ?: previousUserStats["bestTime"]),
            "winCount" to (winCount ?: previousUserStats["winCount"]),
            "lossCount" to (lossCount ?: previousUserStats["lossCount"])
        )
        val fileObject = File(userStatsFilePath)
        fileObject.writeText(GsonBuilder().create().toJson(newUserStats))
    }
}
