package com.example.minesweeper.screens.game

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.minesweeper.R
import com.example.minesweeper.databinding.GameFragmentBinding
import timber.log.Timber

interface Listener {
    fun onTileClick(tileProperty: Map<String, String>)
}

class GameFragment : Fragment(), Listener {

    private lateinit var viewModel: GameViewModel

    private lateinit var binding: GameFragmentBinding

    private lateinit var adapter: GameAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.game_fragment,
            container,
            false
        )

        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        binding.gameViewModel = viewModel
        binding.setLifecycleOwner(this)

        loadView(binding.root)

        return binding.root
    }

    fun loadView(view: View) {
        val recyclerView: RecyclerView = view.findViewById(R.id.game_board) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(context, 9)
        recyclerView.setHasFixedSize(true)

        val cols = 9
        val rows = 9

        val tileGrid = mutableListOf<Map<String, String>>()

        for (i in 0 until rows) {
            for (j in 0 until cols) {
                val tileProperties = mapOf("position" to "$i,$j", "status" to "hidden", "type" to "")
                tileGrid.add(tileProperties)
            }
        }

        adapter = GameAdapter()
        adapter.tileGrid = tileGrid
        adapter.onTileClickListener = this
        recyclerView.adapter = adapter
    }

    override fun onTileClick(tileProperty: Map<String, String>) {
        Timber.i("Tile Clicked: $tileProperty")
    }
}
