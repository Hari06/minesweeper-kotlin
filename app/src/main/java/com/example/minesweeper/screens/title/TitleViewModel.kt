package com.example.minesweeper.screens.title

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.minesweeper.MainActivity
import com.example.minesweeper.UserStats
import timber.log.Timber

class TitleViewModel : ViewModel() {
    private val _lossCount = MutableLiveData<String>()
    val lossCount: LiveData<String>
        get() = _lossCount

    private val _winCount = MutableLiveData<String>()
    val winCount: LiveData<String>
        get() = _winCount

    private val _bestTime = MutableLiveData<String>()
    val bestTime: LiveData<String>
        get() = _bestTime

    var fileName: String = "${MainActivity.getAppContext()!!.filesDir.absolutePath}/userStats.json"

    fun readUserStats() {
        val userStats = UserStats()
        val userData = userStats.readUserStats(fileName)
        Timber.i("User Stats: $userData")

        _lossCount.value = userData["lossCount"].toString()
        _winCount.value = userData["winCount"].toString()
        _bestTime.value = userData["bestTime"].toString()

        Timber.i("Loss Count: ${lossCount.value}")
        Timber.i("Win Count: ${winCount.value}")
        Timber.i("Best Time: ${bestTime.value}")
    }
}
