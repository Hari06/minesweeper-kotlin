package com.example.minesweeper.screens.game

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import timber.log.Timber

class GameViewModel : ViewModel() {

    private val _currentTime = MutableLiveData<Int>()
    val currentTime: LiveData<Int>
        get() = _currentTime

    // The String version of the current time
    var currentTimeString = currentTime

    val remainingMines = 10

    init {
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(object : Runnable {
            var a = 0
            override fun run() {
                a ++
                _currentTime.value = a
                Timber.i("Time starts now $a")
                mainHandler.postDelayed(this, 1000)
            }
        })
    }
}
