package com.example.minesweeper.screens.game

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.minesweeper.R
import timber.log.Timber

class GameAdapter() : RecyclerView.Adapter<GameAdapter.GameViewHolder>() {

    var onTileClickListener: Listener? = null

    var tileGrid = mutableListOf<Map<String, String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tile, parent, false)
        return GameViewHolder(view)
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        holder.bind(getItem(position), onTileClickListener!!)
    }

    override fun getItemCount() = tileGrid.size

    fun getItem(position: Int): Map<String, String> {
        return tileGrid[position]
    }

    class GameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(value: Map<String, String>, onTileClickListener: Listener) {
            itemView.setOnClickListener {
                onTileClickListener.onTileClick(value)
            }

            Timber.i("Value: $value")
        }
    }
}
